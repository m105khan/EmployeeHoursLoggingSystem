To run the program you will need to create two terminal instances.

In one terminal run the following commands after each other  
    1. cd server
    2. node index.js
This will get the backend servers running

To get the front end code, in the other terminal run the following command
    1. npm start

This should get the system up and running.



